#!/bin/bash

export DURATION=${DURATION:-30}
export LAT_THRES=${LAT_THRES:-40}
export SMI_THRES=${SMI_THRES:-40}
export LAT_LOOP=${LAT_LOOP:-1}

export nrcpus=$(grep -c ^processor /proc/cpuinfo)
export rhel_major=$(grep -o '[0-9]*\.[0-9]*' /etc/redhat-release | awk -F '.' '{print $1}')

[ -f $TEST ] && TEST="rt-tests/latency_perf/rt_cyclic_stress"


function runtest()
{

    echo "use cyclictest to measure system latency"
    cmdline="taskset -c $cpu_list cyclictest -n -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_list"
    echo "$cmdline" 
   for i in $(seq 1 $LAT_LOOP); do
        $cmdline | tee cyclic.out
        echo "$i time:" | tee -a lat_cyclic.out
        egrep '(Min|Avg|Max) Latencies' cyclic.out | tee -a lat_cyclic.out
    done

    max_val=$(grep "Max Latencies" lat_cyclic.out | awk -F": " '{print $2}' | tr ' ' '\n' | awk '$0>x {x=$0}; END{print x}')
    max_val=$((10#${max_val}))
    echo "Maximum latency: $max_val" | tee -a max_cyclic.out

    echo "Max Latency with $LAT_LOOP times, echo for $DURATION: $max_val"
    if [ $max_val -gt $LAT_THRES ]; then
        echo "$max_val is higher than $LAT_THRES"
    else
        echo "$max_val is lower than $LAT_THRES"
    fi
}


#--- START ---#
if [ $nrcpus -lt 4 ]; then
    echo "recommend running measure process on >= 4 CPUs machine"
    rstrnt-report-result "$TEST" "SKIP" 0
    exit 0
else
    c_low=$(( nrcpus / 2 ))
    c_high=$(( nrcpus - 1 ))
    cpu_list=$c_low"-"$c_high
    isolate_num=$(( c_high - c_low + 1 ))
fi

    runtest

exit 0
