#!/bin/bash

# 1. Run cyclictest
#cyclictest -l100000000 -m -Sp90 -i200 -h400 -q >output 
#cyclictest -l500000 -m -Sp90 -i200 -h400 -q >output
output=$1
subtittle=$2
echo "Output is $output"
echo "Subtittle is $subtittle"
strcpu=0

# 2. Get maximum latency
max=`grep "Max Latencies" $output | tr " " "\n" | sort -n | tail -1 | sed s/^0*//`

# 3. Grep data lines, remove empty lines and create a common field separator
grep -v -e "^#" -e "^$" $output | tr " " "\t" >histogram 

# 4. Set the number of cores, for example
cores=8

# 5. Create two-column data sets with latency classes and frequency values for each core, for example
for i in `seq 1 $cores`
do
  column=`expr $i + 1`
  cut -f1,$column histogram >histogram$i
done

# 6. Create plot command header
echo -n -e "set title \"Latency plot\"\n\
set label \"Parameters: $subtittle\" at screen 0.45, 0.91 font \"Arial,10\"
set terminal png\n\
set xlabel \"Latency (us), max $max us\"\n\
set logscale y\n\
set xrange [0:80]\n\
set yrange [0.8:*]\n\
set ylabel \"Number of latency samples\"\n\
set output \"plot.png\"\n\
plot " >plotcmd

# 7. Append plot command data references
for i in `seq 1 $cores`
do
  if test $i != 1
  then
    echo -n ", " >>plotcmd
  fi
  cpuno=`expr $i - 1`
# Start CPU count from $strcpu
  if test $strcpu -gt 0 
    then cpuno=`expr $cpuno + $strcpu` 
  fi
  if test $cpuno -lt 10
  then
    title=" CPU$cpuno"
   else
    title="CPU$cpuno"
  fi
  echo -n "\"histogram$i\" using 1:2 title \"$title\" with histeps" >>plotcmd
done

# 8. Execute plot command
gnuplot -persist <plotcmd
