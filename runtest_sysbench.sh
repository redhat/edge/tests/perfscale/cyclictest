#!/bin/bash

export DURATION=${DURATION:-30}
export LAT_THRES=${LAT_THRES:-40}
export SMI_THRES=${SMI_THRES:-40}
export LAT_LOOP=${LAT_LOOP:-1}

export nrcpus=$(grep -c ^processor /proc/cpuinfo)
export rhel_major=$(grep -o '[0-9]*\.[0-9]*' /etc/redhat-release | awk -F '.' '{print $1}')

function runtest_cpu()
{
    taskset -c $cpu_list sysbench cpu run --threads=$isolate_num --time=$DURATION --percentile=99 --cpu-max-prime=200000 |tee sysbench_cpu.out &

    echo "use cyclictest to measure system latency"
    cmdline="taskset -c $cpu_list cyclictest -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_listi -n"
    echo "$cmdline" 
   for i in $(seq 1 $LAT_LOOP); do
        $cmdline | tee cyclic_cpu.out
        echo "$i time:" | tee -a lat_cyclic_cpu.out
        egrep '(Min|Avg|Max) Latencies' cyclic_cpu.out | tee -a lat_cyclic_cpu.out
    done

    max_val=$(grep "Max Latencies" lat_cyclic_cpu.out | awk -F": " '{print $2}' | tr ' ' '\n' | awk '$0>x {x=$0}; END{print x}')
    max_val=$((10#${max_val}))
    echo "Maximum latency: $max_val" | tee -a max_cyclic_cpu.out

    echo "Max Latency with $LAT_LOOP times, echo for $DURATION: $max_val"
    if [ $max_val -gt $LAT_THRES ]; then
        echo "$max_val is higher than $LAT_THRES"
    else
        echo "$max_val is lower than $LAT_THRES"
    fi
}

function runtest_mem()
{

taskset -c $cpu_list sysbench memory run --threads=$isolate_num --memory-block-size=1K --memory-access-mode=rnd   --percentile=99 --memory-oper=read  --memory-total-size=16024G --time=$DURATION |tee sysbench_mem.out &

   echo "use cyclictest to measure system latency"
   cmdline="taskset -c $cpu_list cyclictest -m -q -p95 -D $DURATION -h40 -i 100 -t $isolate_num -a $cpu_list -n"
   echo "$cmdline" 
   for i in $(seq 1 $LAT_LOOP); do
        $cmdline | tee cyclic_mem.out
        echo "$i time:" | tee -a lat_cyclic_mem.out
        egrep '(Min|Avg|Max) Latencies' cyclic_mem.out | tee -a lat_cyclic_mem.out
    done

    max_val=$(grep "Max Latencies" lat_cyclic_mem.out | awk -F": " '{print $2}' | tr ' ' '\n' | awk '$0>x {x=$0}; END{print x}')
    max_val=$((10#${max_val}))
    echo "Maximum latency: $max_val" | tee -a max_cyclic_mem.out

    echo "Max Latency with $LAT_LOOP times, echo for $DURATION: $max_val"
    if [ $max_val -gt $LAT_THRES ]; then
        echo "$max_val is higher than $LAT_THRES"

    else
        echo "$max_val is lower than $LAT_THRES"

    fi
}


#--- START ---#
if [ $nrcpus -lt 4 ]; then
    echo "recommend running measure process on >= 4 CPUs machine"
    exit 0
else
    c_low=$(( nrcpus / 2 ))
    c_high=$(( nrcpus - 1 ))
    cpu_list=$c_low"-"$c_high
    isolate_num=$(( c_high - c_low + 1 ))
fi

    runtest_cpu
    runtest_mem

pkill sysbench; 

exit 0
